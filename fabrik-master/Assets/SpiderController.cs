﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class LegMovement
{
    public Vector3 targgetPos;
    public Vector3 targgetPosStart;
    public float t;

    public bool isValid() { return targgetPos != Vector3.zero; }
    public LegMovement(Vector3 tp, Vector3 tPS)
    {
        targgetPos = tp;
        targgetPosStart = tPS;
        t = 0;
    }
}

public class SpiderController : MonoBehaviour
{

    public List<Transform> targgets;
    public List<Transform> feetFollowBody;

    public AnimationCurve stepShape;

    public List<LegMovement> movement = new List<LegMovement>();

    public float stepLength = 1;
    public float stepHeight = 1;
    public float feetWalkSpeed = 2;

    float bodyHeight = 0;
    Vector3 centerFeet;
    // Start is called before the first frame update
    void Start()
    {

        GameObject targgetEncapsulator;
        targgetEncapsulator = new GameObject();
        targgetEncapsulator.name = gameObject.name + " foot targgets";

        for (int i = 0; i < targgets.Count; i++)
        {
            targgets[i].SetParent(targgetEncapsulator.transform);
            movement.Add(new LegMovement(Vector3.zero, Vector3.zero));
        }

        centerFeet = Vector3.zero;
        for (int i = 0; i < targgets.Count; i++)
        {
            centerFeet += feetFollowBody[i].position;
        }
        centerFeet /= feetFollowBody.Count;
        bodyHeight = Vector3.Project(transform.position - centerFeet, transform.up).magnitude;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey(KeyCode.W)) transform.position -= transform.right*Time.deltaTime* feetWalkSpeed;
        if (Input.GetKey(KeyCode.S)) transform.position += transform.right * Time.deltaTime * feetWalkSpeed;
        if (Input.GetKey(KeyCode.A)) transform.position -= transform.forward * Time.deltaTime * feetWalkSpeed;
        if (Input.GetKey(KeyCode.D)) transform.position += transform.forward * Time.deltaTime * feetWalkSpeed;
    }


    bool lastLegActive = false;
    public LayerMask layerMask;
    Vector3 GetNormal(Vector3 a, Vector3 b, Vector3 c)
    {
        // Find vectors corresponding to two of the sides of the triangle.
        Vector3 side1 = b - a;
        Vector3 side2 = c - a;

        // Cross the vectors to get a perpendicular vector, then normalize it.
        return Vector3.Cross(side1, side2).normalized;
    }
    void LateUpdate()
    {
        centerFeet = Vector3.zero;
        for (int i = 0; i < feetFollowBody.Count; i++)
        {
            centerFeet += feetFollowBody[i].position;
        }
        centerFeet /= feetFollowBody.Count;
        //place body height and rotation
        float h = Vector3.Project(transform.position - centerFeet, transform.up).magnitude;

        if (Mathf.Abs(bodyHeight - h) > 0.1f)
        {
            transform.position += transform.up.normalized * (bodyHeight - h);
        }

        Vector3 normal = Vector3.zero;
        for (int i = 0; i < feetFollowBody.Count; i++)
        {
            if (i == 2 || i == 5) continue;
            Vector3 v;
            if (i < feetFollowBody.Count / 2)
            {
                v = GetNormal(centerFeet, feetFollowBody[i].position, feetFollowBody[(i + 1) % feetFollowBody.Count].position);

                Vector3 center = centerFeet + feetFollowBody[i].position + feetFollowBody[(i + 1) % feetFollowBody.Count].position;
                center /= 3;
                Color c = new Color(0, (float)i / feetFollowBody.Count, 0, 1);
                Debug.DrawRay(feetFollowBody[i].position, centerFeet - feetFollowBody[i].position, c);
                Debug.DrawRay(feetFollowBody[(i + 1) % feetFollowBody.Count].position, centerFeet - feetFollowBody[(i + 1) % feetFollowBody.Count].position, c);
                Debug.DrawRay(feetFollowBody[(i + 1) % feetFollowBody.Count].position, feetFollowBody[i].position - feetFollowBody[(i + 1) % feetFollowBody.Count].position, c);

                Debug.DrawRay(center, v * 10, new Color(0, (float)i / feetFollowBody.Count, 0,1));
            }
            else
            {
                v = GetNormal(centerFeet, feetFollowBody[(i + 1) % feetFollowBody.Count].position, feetFollowBody[i].position);
                Vector3 center = centerFeet + feetFollowBody[i].position + feetFollowBody[(i + 1) % feetFollowBody.Count].position;
                center /= 3;
                Color c = new Color(0, 0, (float)i / feetFollowBody.Count, 1);
                 Debug.DrawRay(feetFollowBody[i].position, centerFeet - feetFollowBody[i].position, c);
                Debug.DrawRay(feetFollowBody[(i + 1) % feetFollowBody.Count].position, centerFeet - feetFollowBody[(i + 1) % feetFollowBody.Count].position, c);
                Debug.DrawRay(feetFollowBody[(i + 1) % feetFollowBody.Count].position, feetFollowBody[i].position - feetFollowBody[(i + 1) % feetFollowBody.Count].position, c);
                Debug.DrawRay(center, v * 10, new Color(0, 0, (float)i / feetFollowBody.Count, 1));
            }

            normal += v;
        }
        Debug.DrawRay(transform.position, (normal).normalized * 10, Color.red);
        transform.up = Vector3.RotateTowards( transform.up, (normal / feetFollowBody.Count).normalized, Time.deltaTime, 0.0f);


        //move legs
        for (int i = 0; i < targgets.Count; i++)
        {
            //if (i == 3) lastLegActive = false;

            if(Physics.Raycast(feetFollowBody[i].position + transform.up*5, -transform.up, out RaycastHit hit, Mathf.Infinity, layerMask))
            {

                feetFollowBody[i].position = hit.point;
            }

            if(Vector3.Distance(targgets[i].position, feetFollowBody[i].position)> stepLength && !movement[i].isValid() && !lastLegActive)
            {
                movement[i].targgetPos = targgets[i].position + (feetFollowBody[i].position - targgets[i].position)*1.9f;// this could be mult by the spider current speed
                if (Physics.Raycast(movement[i].targgetPos, -transform.up, out RaycastHit targgHit, Mathf.Infinity, layerMask))
                {

                    movement[i].targgetPos = hit.point;
                }
                movement[i].targgetPosStart = targgets[i].position;// this could be mult by the spider current speed
                movement[i].t = 0;

            }
            if (movement[i].isValid())
            {
                if (movement[i].t<1)
                {
                    Vector3 dir = (movement[i].targgetPos - movement[i].targgetPosStart);
                    targgets[i].position = movement[i].targgetPosStart + (dir * movement[i].t) + (transform.up * stepShape.Evaluate(movement[i].t) * stepHeight);
                    movement[i].t += Time.deltaTime * feetWalkSpeed;
                }
                else
                {
                    movement[i].targgetPos = Vector3.zero;
                }
            }
            lastLegActive = movement[i].isValid();
        }

 

    }
}
