using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class IK_FABRIK2 : MonoBehaviour
{
    public Transform[] joints;
    public Transform foot;
    GameObject targ;
    GameObject targ1;
    public Transform target;
    public Transform body;

    private Vector3[] copy;
    private float[] distances;
    private bool done;
    private Vector3 originalBase;
    private Vector3 originalBaseOrientation;
    GameObject g;
    void Start()
    {
        distances = new float[joints.Length - 1];
        for (int i = 0; i < joints.Length - 1; i++)
            distances[i] = Vector3.Distance(joints[i].position, joints[i + 1].position);

        copy = new Vector3[joints.Length];
        originalBase = copy[0];

        originalBaseOrientation = joints[1].position - joints[0].position;
    }

    void FixedUpdate()
    {

        // Copy the joints positions to work with
        for (int i = 0; i < joints.Length; i++)
            copy[i] = joints[i].position;

        originalBase = copy[0];
        Vector3 v = (body.rotation * originalBaseOrientation);
        Debug.DrawRay(originalBase, v, Color.cyan);


        done = Vector3.Distance(copy[copy.Length - 1], target.position) < 0.01f;

        if (!done)
        {
            float targetRootDist = Vector3.Distance(copy[0], target.position);

            // Update joint positions
            if (targetRootDist > distances.Sum())
            {
                // The target is unreachable
                Vector3 newTarg = copy[0] + ((target.position - copy[0]).normalized * distances.Sum());
                //fabrik(newTarg);
                fabrik(newTarg);

            }
            else
            {
                // The target is reachable
                //fabrik(target.position);
                fabrik(target.position);
            }

            // Update original joint rotations
            for (int i = 0; i <= joints.Length - 2; i++)
            {

                joints[i].up = -(copy[i] - copy[i + 1]).normalized;
                joints[i].position = copy[i];

            }
        }
        if (foot != null)
        {
            foot.parent.up = -transform.root.up;
        }

    }

    public float[] maxAngles;
    int maxTries = 100;
    void fabrik(Vector3 _target)
       
    {
        int tries = 0;
        while (Vector3.Distance(copy[copy.Length - 1], _target) > 0.1f && tries<maxTries)
        {
            tries++;
            // STAGE 1: FORWARD REACHING
            copy[copy.Length - 1] = _target;
            for (int i = copy.Length - 2; i >= 0; i--)
            {
                Vector3 meToNext = (copy[i + 1] - copy[i]).normalized,
                    lastToMe = i == 0 ? originalBaseOrientation : (copy[i] - copy[i - 1]).normalized;

                Debug.DrawRay(copy[i], meToNext, Color.green);
                Debug.DrawRay(i == 0 ? originalBase : copy[i - 1], lastToMe, Color.red);

                if (Vector3.Angle(meToNext, lastToMe) > maxAngles[i])// angle too open
                {
                    Debug.Log(Vector3.Angle(meToNext, lastToMe) + " is Bigger than " + maxAngles[i]);
                    Vector3 rotationAxis = Vector3.Cross(meToNext, lastToMe);
                    Vector3 direction = Quaternion.AngleAxis(maxAngles[i], rotationAxis)* meToNext;
                    copy[i] = copy[i + 1] + direction.normalized * distances[i];
                }
                else//ok
                {
                    copy[i] = copy[i + 1] + (copy[i] - copy[i + 1]).normalized * distances[i];

                }  
            }

            // STAGE 2: BACKWARD REACHING
            copy[0] = originalBase;
            for (int i = 0; i < copy.Length - 2; i++)
            {
                Vector3 meToNext = (copy[i + 1] - copy[i]).normalized,
                    lastToMe = i == 0 ? originalBaseOrientation : (copy[i] - copy[i - 1]).normalized;

                if (Vector3.Angle(meToNext, lastToMe) > maxAngles[i])// angle too open
                {
                    Debug.Log(Vector3.Angle(meToNext, lastToMe) + " is Bigger than " + maxAngles[i]);
                    Vector3 rotationAxis = Vector3.Cross(meToNext, lastToMe);
                    Vector3 direction = Quaternion.AngleAxis(maxAngles[i], rotationAxis) * meToNext;

                    copy[i + 1] = copy[i] + direction.normalized * distances[i];
                }
                else//ok
                {
                    copy[i + 1] = copy[i] + (copy[i + 1] - copy[i]).normalized * distances[i];
                }
            }
        }
    }

  

}
